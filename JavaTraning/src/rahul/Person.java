package rahul;

public class Person {
	private String name;
	private String email;
	private String designation;
	private int ssn;

	public Person() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Person(String name, String email, String designation, int ssn) {
		super();
		this.name = name;
		this.email = email;
		this.designation = designation;
		this.ssn = ssn;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDesignation() {
		return designation;
	}

	public void setdesignation(String designation) {
		this.designation = designation;
	}

	public int getSsn() {
		return ssn;
	}

	public void setSsn(int ssn) {
		this.ssn = ssn;
	}

}
