package shanky;
//Comparable
public class ShashankProduct implements Comparable<ShashankProduct> {
	private String ProductName;
	private Integer ProductPrice;
	private String ProductSpecs;
	private Integer MfgYear;
	
	public ShashankProduct(String productName, Integer productPrice, String productSpecs, Integer mfgYear) {
		super();
		ProductName = productName;
		ProductPrice = productPrice;
		ProductSpecs = productSpecs;
		MfgYear = mfgYear;
	}
	

	public ShashankProduct() {
		super();
		// TODO Auto-generated constructor stub
	}
	


	public String getProductName() {
		return ProductName;
	}


	public void setProductName(String productName) {
		ProductName = productName;
	}


	public Integer getProductPrice() {
		return ProductPrice;
	}


	public void setProductPrice(Integer productPrice) {
		ProductPrice = productPrice;
	}


	public String getProductSpecs() {
		return ProductSpecs;
	}


	public void setProductSpecs(String productSpecs) {
		ProductSpecs = productSpecs;
	}


	public Integer getMfgYear() {
		return MfgYear;
	}


	public void setMfgYear(Integer mfgYear) {
		MfgYear = mfgYear;
	}


	@Override
	public int compareTo(ShashankProduct o) {
		return (this.ProductPrice - o.ProductPrice);
	}


	@Override
	public String toString() {
		return "ShashankProduct [ProductName=" + ProductName + ", ProductPrice=" + ProductPrice + ", ProductSpecs="
				+ ProductSpecs + ", MfgYear=" + MfgYear + "]";
	}

}
