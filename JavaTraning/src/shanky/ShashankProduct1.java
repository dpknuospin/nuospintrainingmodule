package shanky;
// Comparator
public class ShashankProduct1 {
	private String ProductName;
	private Integer ProductPrice;
	private String ProductSpecs;
	private String MfgYear;
	

	public ShashankProduct1(String productName, Integer productPrice, String productSpecs, String mfgYear) {
		super();
		ProductName = productName;
		ProductPrice = productPrice;
		ProductSpecs = productSpecs;
		MfgYear = mfgYear;
	}
	


	public ShashankProduct1() {
		super();
		// TODO Auto-generated constructor stub
	}
	



	public String getProductName() {
		return ProductName;
	}



	public void setProductName(String productName) {
		ProductName = productName;
	}



	public Integer getProductPrice() {
		return ProductPrice;
	}



	public void setProductPrice(Integer productPrice) {
		ProductPrice = productPrice;
	}



	public String getProductSpecs() {
		return ProductSpecs;
	}



	public void setProductSpecs(String productSpecs) {
		ProductSpecs = productSpecs;
	}



	public String getMfgYear() {
		return MfgYear;
	}



	public void setMfgYear(String mfgYear) {
		MfgYear = mfgYear;
	}



	@Override
	public String toString() {
		return "ShashankProduct1 [ProductName=" + ProductName + ", ProductPrice=" + ProductPrice + ", ProductSpecs="
				+ ProductSpecs + ", MfgYear=" + MfgYear + "]";
	}



	

}
