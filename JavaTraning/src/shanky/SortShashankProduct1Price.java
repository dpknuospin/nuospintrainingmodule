package shanky;

import java.util.Comparator;

public class SortShashankProduct1Price implements Comparator<ShashankProduct1> {

	@Override
	public int compare(ShashankProduct1 o1, ShashankProduct1 o2) {
		
		return o1.getProductPrice() -o2.getProductPrice();
	}

}
