package shanky;
import java.util.ArrayList;
import java.util.Collections;

import com.nuospin.javaTraining.collection.SortProduct1Price;

public class ShashankProBuilder1 {
	public static void main(String args[]){
	ShashankProduct1 product1= new ShashankProduct1("Lassi" , 20, "Rich and creamy" , "022016");
	ShashankProduct1 product2= new ShashankProduct1("Softdrink" , 40, "Soda" , "042016");
	ShashankProduct1 product3= new ShashankProduct1("ColdCoffe" , 80, "Mocca" , "052016");
	ArrayList<ShashankProduct1> ProductSet= new ArrayList<ShashankProduct1>();
	ProductSet.add(product1);
	ProductSet.add(product2);
	ProductSet.add(product3);
	Collections.sort(ProductSet, new SortShashankProduct1Price());
	System.out.println(ProductSet);

}
}
