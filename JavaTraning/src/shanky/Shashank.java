package shanky;

public class Shashank {
		private String name;
		private String emailId;
		private String location;
		private Long phone;
		public Shashank() {
			super();
			// TODO Auto-generated constructor stub
		}
		public Shashank(String name, String emailId, String location, Long phone) {
			super();
			this.name = name;
			this.emailId = emailId;
			this.location = location;
			this.phone = phone;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getEmailId() {
			return emailId;
		}
		public void setEmailId(String emailId) {
			this.emailId = emailId;
		}
		public String getLocation() {
			return location;
		}
		public void setLocation(String location) {
			this.location = location;
		}
		public Long getPhone() {
			return phone;
		}
		public void setPhone(Long phone) {
			this.phone = phone;
		}

}
