package SachinTest.LoginPack;

public class LoginServiceMainDemo {

	public static void main(String[] args) {

		LoginInterface GoogleLogin = LoginFactory.getLogin("google");
		GoogleLogin.login("SachinGoel", "sachin1234");
		
		LoginInterface FBLogin = LoginFactory.getLogin("facebook");
		FBLogin.login("SachinGoelFB", "sachin1234");
		
		LoginInterface TwitterLogin = LoginFactory.getLogin("twitter");
		TwitterLogin.login("SachinGoelTwitter", "sachin1234");
		
	}

}
