package SachinTest.LoginPack;

public class LoginFactory {

	public static LoginInterface getLogin(String LoginFrom){
		
		if(LoginFrom==null){
			return null;
		}
		
		if(LoginFrom.equalsIgnoreCase("google")){
			return new GoogleService();
		}
		
		else if (LoginFrom.equalsIgnoreCase("facebook")){
			
			return new FacebookService();
		}
		
		else if(LoginFrom.equalsIgnoreCase("twitter")){
			
			return new TwitterService();
		}
		
		return null;
	}

}
