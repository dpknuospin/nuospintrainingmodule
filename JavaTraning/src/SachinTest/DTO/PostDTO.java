package SachinTest.DTO;

import java.util.*;

import SachinTest.Service.CommentInterface;
import SachinTest.Service.LikeInterface;

;
public class PostDTO implements CommentInterface , LikeInterface {


	private static final Object PostDTO = null;//  what is the significance of this line of code
	//private static final PostDTO PostDTO = null;
	private Long Pid;
	private Long UserId;
	private String Descrption;
	private Date CreatedDate;
	
	private Map<PostDTO,ArrayList<String>> commentMap;
	private  ArrayList<String> commentString;
	
	private Integer i=1;
	private Map<PostDTO,Integer> likeMap;
	
	
	public PostDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PostDTO(Long pid, Long userId, String descrption, Date createdDate) {
		super();
		Pid = pid;
		UserId = userId;
		Descrption = descrption;
		CreatedDate = createdDate;
	}

	public Long getPid() {
		return Pid;
	}

	public void setPid(Long pid) {
		Pid = pid;
	}

	public Long getUserId() {
		return UserId;
	}

	public void setUserId(Long userId) {
		UserId = userId;
	}

	public String getDescrption() {
		return Descrption;
	}

	public void setDescrption(String descrption) {
		Descrption = descrption;
	}

	public Date getCreatedDate() {
		return CreatedDate;
	}

	public void setCreatedDate(Date createdDate) {
		CreatedDate = createdDate;
	}
	
	
	public Boolean PostValidation(String description)
	{
		
		if(validateDescription(description))
			return true;
		else return false;
		
	}
	
	
	public Boolean validateDescription(String des)
	{
		if(des!= null && des.length()>0)
			return true;
		return false;
	}

	
	@Override
	public Map<PostDTO, ArrayList<String>> CreateComment(String abc) {
		//commentMap.put(this.PostDTO, commentString.add(abc));
		return commentMap;
	}
	

	@Override
	public Integer CountComment(ArrayList<String> AlS) {
		ArrayList<String> cString= commentMap.get(PostDTO);
		return cString.size();
	}

	
	public Map<PostDTO, Integer> PostLike() {
		if(likeMap.get(this.PostDTO) == null || likeMap.get(this.PostDTO) == 0)
		{
			likeMap.put((PostDTO) this.PostDTO, i);
		}
		
		else if(likeMap.get(this.PostDTO) != null || likeMap.get(this.PostDTO) != 0)
		{
		
			likeMap.put((PostDTO) this.PostDTO, i++);
			
		}

		return likeMap;
	}

	public Map<PostDTO, Integer> PostDislike() {
		if(likeMap.get(this.PostDTO) != null || likeMap.get(this.PostDTO) != 0)
		{
		Integer i1	= likeMap.get(this.PostDTO);
			
			likeMap.put((PostDTO) this.PostDTO, i1--);
		}
		
		return likeMap;
	}

	@Override
	public Integer LikeCount() {
		
		return likeMap.get(this.PostDTO);
	}
	
	



	
		
	
	
}

