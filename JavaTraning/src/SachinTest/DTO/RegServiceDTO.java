package SachinTest.DTO;

public class RegServiceDTO {

	
	private static String username;
	private static String emailId;
	private static String password;
	private static String s2;
	
	public RegServiceDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public RegServiceDTO(String username, String emailId, String password) {
		super();
		RegServiceDTO.username = username;
		RegServiceDTO.emailId = emailId;
		RegServiceDTO.password = password;
	}

	public static String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		RegServiceDTO.username = username;
	}

	public static String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		RegServiceDTO.emailId = emailId;
	}

	public static String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		RegServiceDTO.password = password;
	}
	
	
	public static boolean Validate(RegServiceDTO regserviceDto)
	{
		if (nameValidation(RegServiceDTO.getUsername()) && passwordValidation(RegServiceDTO.getPassword())
				&& emailValidation(RegServiceDTO.getEmailId())) {
			return true;
		}
		return false;

	}	
	
		public static boolean nameValidation(String Username){
			return checkfornull(Username);
		}
		
		
		public static boolean passwordValidation(String pass){
			return checkfornull(pass);
		}
		
	
			public static boolean emailValidation(String email)
			{
			return checkfornull(email);
		}
			
			public static boolean checkfornull(String s)
			{
				s2 = s;
				if(s2 != null || s2.length() > 0)
					return true;
				else 
					return false;
			}
			

}
