package SachinTest.Service;

import SachinTest.EmptyFieldException;
import SachinTest.DTO.RegServiceDTO;

public class RegService {
		
	public RegServiceDTO register(RegServiceDTO regserviceDto)
	{
		//RegServiceDTO RegServiceDtoToObj = new RegServiceDTO();
		if(RegServiceDTO.Validate(regserviceDto)){
			
			return SaveToDB(regserviceDto);
		}
		
		else{
			
			throw new EmptyFieldException("Please enter all the required fields");
		}
		
	
	}
	
	public RegServiceDTO SaveToDB(RegServiceDTO regserviceDtoDB)
	{
		//Code for saving the user entry in the DataBase
		return regserviceDtoDB;
		
	}

	
	

}
