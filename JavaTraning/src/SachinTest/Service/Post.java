package SachinTest.Service;


import java.util.ArrayList;
import java.util.Date;

public class Post {

	
	
	private Long Pid;
	private String description;
	private Date CreatedDate;
	private Date Modifieddate;
	private String AttachmentURL;
	private long UserId;
	
	

	public Post() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Post(Long pid, String description, Date createdDate, Date modifieddate, String attachmentURL, long userId) {
		super();
		Pid = pid;
		this.description = description;
		CreatedDate = createdDate;
		Modifieddate = modifieddate;
		AttachmentURL = attachmentURL;
		UserId = userId;
	}

	public Long getPid() {
		return Pid;
	}

	public void setPid(Long pid) {
		Pid = pid;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCreatedDate() {
		return CreatedDate;
	}

	public void setCreatedDate(Date createdDate) {
		CreatedDate = createdDate;
	}

	public Date getModifieddate() {
		return Modifieddate;
	}

	public void setModifieddate(Date modifieddate) {
		Modifieddate = modifieddate;
	}

	public String getAttachmentURL() {
		return AttachmentURL;
	}

	public void setAttachmentURL(String attachmentURL) {
		AttachmentURL = attachmentURL;
	}

	public long getUserId() {
		return UserId;
	}

	public void setUserId(long userId) {
		UserId = userId;
	}

	@Override
	public String toString() {
		return "Post [Pid=" + Pid + ", description=" + description + ", CreatedDate=" + CreatedDate + ", Modifieddate="
				+ Modifieddate + ", AttachmentURL=" + AttachmentURL + ", UserId=" + UserId + "]";
	}
	

	
	
	
}