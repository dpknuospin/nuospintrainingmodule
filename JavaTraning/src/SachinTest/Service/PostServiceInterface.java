package SachinTest.Service;

import SachinTest.DTO.PostDTO;

public interface PostServiceInterface {

	public  PostDTO GeneratePost(PostDTO postDto);
	
}
