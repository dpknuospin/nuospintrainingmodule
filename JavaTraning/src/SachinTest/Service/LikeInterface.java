package SachinTest.Service;
import SachinTest.DTO.PostDTO;
import java.util.*;


public interface LikeInterface {

	public Map<PostDTO,Integer> PostLike();
	
	public Map<PostDTO,Integer> PostDislike();
	
	public Integer LikeCount();
	
}
