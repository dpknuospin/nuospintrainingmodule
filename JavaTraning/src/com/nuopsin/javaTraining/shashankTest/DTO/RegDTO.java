package com.nuopsin.javaTraining.shashankTest.DTO;

public class RegDTO {
	public String emailAddress;
	public String password;
	public RegDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public RegDTO(String emailAdrress, String password) {
		super();
		this.emailAddress = emailAdrress;
		this.password = password;
	}

	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAdrress(String emailAdrress) {
		this.emailAddress = emailAdrress;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "RegDTO [emailAdrress=" + emailAddress + ", password=" + password + "]";
	}

	public Boolean ValidateRegister(String emailAddress, String password) {
		if (ValidateEmail(getEmailAddress()) && ValidatePassword(getPassword())) {
			return true;
		}
		return false;
	}
		boolean ValidateEmail(String email) {
			return StringNullOrEmpty(email);
		}

		boolean ValidatePassword(String password) {
			return StringNullOrEmpty(password);
		}

				private Boolean StringNullOrEmpty(String string) {
			if (string != null && string.length() > 0) {
				return true;
			}
			return false;
	}

			
}
