package com.nuospin.javaTraining.shashankTest.Service;

import com.nuospin.javaTraining.shashankTest.Exception.CouldNotFindUserException;

public interface FriendInterface {
public Boolean FriendRequest(Long id, Long friendId) throws CouldNotFindUserException;
}
