package com.nuospin.javaTraining.shashankTest.Service;
import com.nuospin.javaTraining.shashankTest.Exception.CouldNotFindUserException;
import com.nuospin.javaTraining.shashankTest.domain.User;
public class FriendInterfaceImpl implements FriendInterface{

	public Boolean FriendRequest(Long id, Long friendId) throws CouldNotFindUserException {
		User user= findUserById(id);
		User friend= findUserById(friendId);
		if( user != null && friend != null)
		{
			// code for sending friend request to user
		}
		throw new CouldNotFindUserException ("User is not found");
	}

	public User findUserById(Long id) {
		// code for  finding and returning user from database
		return null;
	}
	

}
