package com.nuospin.javaTraining.shashankTest.Service;

public class FactoryLogin {
	public static ServiceLogin getInstence(String type){
		if(type == null){
			return null;
		}
		if(type.equalsIgnoreCase("facebook")){
			return new FacebookLogin();
		}
		else if (type.equalsIgnoreCase("google")) {
			return new GoogleLogin();
			
		}
		return null;
		
	}

}
