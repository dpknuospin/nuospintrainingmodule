package com.nuospin.javaTraining.shashankTest.Service;

import javax.activity.InvalidActivityException;

import com.nuopsin.javaTraining.shashankTest.DTO.LoginDTO;
import com.nuopsin.javaTraining.shashankTest.DTO.RegDTO;
import com.nuospin.javaTraining.shashankTest.Exception.LoginFailException;
import com.nuospin.javaTraining.shashankTest.domain.User;

public interface ServiceLogin {
	default public RegDTO register(RegDTO registrationDto) throws InvalidActivityException{
		 if(registrationDto.ValidateRegister(registrationDto.getEmailAddress(), registrationDto.getPassword())){
			 //save registration details  into database and return saved details.
			 return registrationDto;
		 }
		throw new InvalidActivityException("Missing required fields");
	 }

	public User login(LoginDTO loginDto) throws LoginFailException;
}
