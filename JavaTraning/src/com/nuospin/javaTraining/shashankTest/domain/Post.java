package com.nuospin.javaTraining.shashankTest.domain;
import java.util.Date;

public class Post {
	private Long Pid;
	private String Description;
	private Date CreatedDate;
	private String AttachmentLink;
	private long UserId;
	
	public Post() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Post(Long pid, String Description, Date createdDate, Date modifieddate, String attachmentLink, long userId) {
		super();
		Pid = pid;
		this.Description = Description;
		CreatedDate = createdDate;
		AttachmentLink = attachmentLink;
		UserId = userId;
	}

	public Long getPid() {
		return Pid;
	}

	public void setPid(Long pid) {
		Pid = pid;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		this.Description = description;
	}

	public Date getCreatedDate() {
		return CreatedDate;
	}

	public void setCreatedDate(Date createdDate) {
		CreatedDate = createdDate;
	}

	public String getAttachmentLink() {
		return AttachmentLink;
	}

	public void setAttachmentLink(String attachmentLink) {
		AttachmentLink= attachmentLink;
	}

	public long getUserId() {
		return UserId;
	}

	public void setUserId(long userId) {
		UserId = userId;
	}

	@Override
	public String toString() {
		return "Post [Pid=" + Pid + ", Description=" + Description + ", CreatedDate=" + CreatedDate + " , AttachmentLink=" + AttachmentLink + ", UserId=" + UserId + "]";
	}
}
