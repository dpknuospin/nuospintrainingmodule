package com.nuospin.javaTraining.shashankTest.Exception;

public class CouldNotFindUserException extends Exception{
	public CouldNotFindUserException() {
		super();
	}

	public CouldNotFindUserException(String message) {
		super(message);
		System.out.println(message);
	

}
}
