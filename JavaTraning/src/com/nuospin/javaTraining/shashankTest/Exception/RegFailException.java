package com.nuospin.javaTraining.shashankTest.Exception;

public class RegFailException extends Exception {
	public RegFailException() {
		super();
		System.out.println("Registration details incorrect");
	}

	public RegFailException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
		System.out.println(message);
	}
	

}
