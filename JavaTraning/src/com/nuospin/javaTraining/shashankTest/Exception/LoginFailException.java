package com.nuospin.javaTraining.shashankTest.Exception;

public class LoginFailException extends Exception {
	
		public LoginFailException() {
			super();
		}

		public LoginFailException(String message) {
			super(message);
			System.out.println(message);
		}

	}
