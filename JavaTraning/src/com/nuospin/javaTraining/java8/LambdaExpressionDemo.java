package com.nuospin.javaTraining.java8;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class LambdaExpressionDemo {
	public static void runAdd(){
		AddInterface addInterface =  (int a, int b)-> {return a+b;};
		System.out.println(addInterface.add(2,3));
		
		System.out.println(addInterface.sum());
		
	}
	

	public static void main(String[] args) {
			runAdd();
			List<Integer> numbers = Arrays.asList(3, 2, 2, 3, 7, 3, 5);
			//get list of unique squares
			List<Integer> squaresList = numbers.stream().map( i -> i*i).distinct().collect(Collectors.toList());
			List<Integer> oddNumbers   = numbers.stream().filter(i -> i%2 != 0).distinct().collect(Collectors.toList());
			System.out.println(squaresList);
			System.out.println(oddNumbers);
			List<Integer> first4elements = Arrays.asList(3, 2,4,5,6).stream().limit(4).collect(Collectors.toList());
			
			System.out.println(first4elements);
			
			
			
			
	}

}
