package com.nuospin.javaTraining.java8.methodReference;

import java.util.Arrays;
import java.util.List;

public class MethodReferenceDemo {
	private  String getName(String name){
		return name;
	}

	public static void main(String[] args) {
		MethodRefrenceInterface ref =new  MethodReferenceDemo()::getName;
		System.out.println(ref.methodRefrenceHandler("deepak"));
		List<String> list = Arrays.asList("as","sd","fg");
		list.forEach(System.out::println);

	}

}
