package com.nuospin.javaTraining.java8;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class LambdaExpExample {

	public static void main(String[] s) throws NumberFormatException, IOException
	{
		int n=0;
		BufferedReader ob= new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter size of array");
		n= Integer.parseInt(ob.readLine());
		int a1[] =new int[n];
		System.out.println("Enter elements");
		for(int i=0;i<n;i++)
		{
			try {
				a1[i]=Integer.parseInt(ob.readLine());
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		for(int j=0;j<n;j++)
		{
			System.out.println(a1[j]);
		}
		/*
		List<Integer> numbers = Arrays.asList(a1[]);
		System.out.println(numbers.size());
		List<Integer> squaresList = numbers.stream().map( i -> i*i).distinct().collect(Collectors.toList());
		*/
	}
	
}
