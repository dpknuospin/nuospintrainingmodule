package com.nuospin.javaTraining.sachinTest.Domain.DTO;

public class RegistrationDTO {

	private  String username;
	private  String emailId;
	private  String password;
	private  String s2;
	
	public RegistrationDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public RegistrationDTO(String username, String emailId, String password) {
		super();
		this.username = username;
		this.emailId = emailId;
		this.password = password;
	}

	public   String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public   String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public   String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	
	public  boolean Validate(String username, String password,String userEmail)
	{
		if (nameValidation(username) && passwordValidation(password)
				&& emailValidation(userEmail)) {
			return true;
		}
		return false;

	}	
	
		public  boolean nameValidation(String Username){
			return checkfornull(Username);
		}
		
		
		public  boolean passwordValidation(String pass){
			return checkfornull(pass);
		}
		
	
			public  boolean emailValidation(String email)
			{
			return checkfornull(email);
		}
			
			public  boolean checkfornull(String s)
			{
				s2 = s;
				if(s2 != null || s2.length() > 0)
					return true;
				else 
					return false;
			}

		
}
