package com.nuospin.javaTraining.sachinTest.Domain.DTO;

import java.util.Date;

public class PostDTO {

	private Long Pid;
	private Long UserId;
	private String Descrption;
	private Date CreatedDate;
	
	public PostDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public PostDTO(Long pid, Long userId, String descrption, Date createdDate) {
		super();
		Pid = pid;
		UserId = userId;
		Descrption = descrption;
		CreatedDate = createdDate;
	}
	public Long getPid() {
		return Pid;
	}
	public void setPid(Long pid) {
		Pid = pid;
	}
	public Long getUserId() {
		return UserId;
	}
	public void setUserId(Long userId) {
		UserId = userId;
	}
	public String getDescrption() {
		return Descrption;
	}
	public void setDescrption(String descrption) {
		Descrption = descrption;
	}
	public Date getCreatedDate() {
		return CreatedDate;
	}
	public void setCreatedDate(Date createdDate) {
		CreatedDate = createdDate;
	}
	
	public boolean validatePost(String description)
	{
		if(DescriptionValidate(description))
			return true;
		else return false;
		
	}
	
	public boolean DescriptionValidate(String s)
	{
		
		if(s.length()>0 && s!= null)
			return true;
		else return false;
	}
	
}
