package com.nuospin.javaTraining.sachinTest.Domain.Pojo;


import java.util.*;

import com.nuospin.javaTraining.sachinTest.Domain.Pojo.Enum.Gender;
import com.nuospin.javaTraining.sachinTest.Domain.Pojo.objectField.Address;

import SachinTest.DTO.PostDTO;

public class User {

	private String username ;
	private String emailId;
	private String FirstName;
	private String Lastname;
	private String password;
	private Address address;
	private Gender gender;
	
	private Set<Long> postId = new HashSet<Long>();
	
	
	
	private static ArrayList<PostDTO> pdto = new ArrayList<PostDTO>();
	
	public User() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	

	public User(String username, String emailId, String firstName, String lastname, String password, Address address,
			Gender gender) {
		super();
		this.username = username;
		this.emailId = emailId;
		FirstName = firstName;
		Lastname = lastname;
		this.password = password;
		this.address = address;
		this.gender = gender;
	}

	
	
	public Set<Long> getPostId() {
		return postId;
	}



	public void setPostId(Set<Long> postId) {
		this.postId = postId;
	}



	public static ArrayList<PostDTO> getPdto() {
		return pdto;
	}

	public static void setPdto(ArrayList<PostDTO> pdto) {
		User.pdto = pdto;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getFirstName() {
		return FirstName;
	}

	public void setFirstName(String firstName) {
		FirstName = firstName;
	}

	public String getLastname() {
		return Lastname;
	}

	public void setLastname(String lastname) {
		Lastname = lastname;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}
	
	
	
	
	
	
	
}