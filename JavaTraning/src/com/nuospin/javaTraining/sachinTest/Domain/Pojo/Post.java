package com.nuospin.javaTraining.sachinTest.Domain.Pojo;

import java.util.*;

public class Post {

	private Long Pid;
	private Long UserId;
	private String Descrption;
	private Date CreatedDate;
	
	

	private Map<Post,ArrayList<String>> commentMap;
	private Map<Post,Integer> likeMap;
	
	
	public Post() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Post(Long pid, Long userId, String descrption, Date createdDate, Map<Post, ArrayList<String>> commentMap,
			Map<Post, Integer> likeMap) {
		super();
		Pid = pid;
		UserId = userId;
		Descrption = descrption;
		CreatedDate = createdDate;
		this.commentMap = commentMap;
		this.likeMap = likeMap;
	}

	public Long getPid() {
		return Pid;
	}

	public void setPid(Long pid) {
		Pid = pid;
	}

	public Long getUserId() {
		return UserId;
	}

	public void setUserId(Long userId) {
		UserId = userId;
	}

	public String getDescrption() {
		return Descrption;
	}

	public void setDescrption(String descrption) {
		Descrption = descrption;
	}

	public Date getCreatedDate() {
		return CreatedDate;
	}

	public void setCreatedDate(Date createdDate) {
		CreatedDate = createdDate;
	}

	public Map<Post, ArrayList<String>> getCommentMap() {
		return commentMap;
	}

	public void setCommentMap(Map<Post, ArrayList<String>> commentMap) {
		this.commentMap = commentMap;
	}

	public Map<Post, Integer> getLikeMap() {
		return likeMap;
	}

	public void setLikeMap(Map<Post, Integer> likeMap) {
		this.likeMap = likeMap;
	}
	
	
	
}
