package com.nuospin.javaTraining.sachinTest.service;

import java.util.*;

import com.nuospin.javaTraining.sachinTest.Domain.Pojo.Post;

public interface LikeInterface {

public Map<Post,Integer> PostLike();
	
	public Map<Post,Integer> PostDislike();
	
	public Integer LikeCount();
}
