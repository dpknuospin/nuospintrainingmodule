package com.nuospin.javaTraining.sachinTest.service;

import com.nuospin.javaTraining.sachinTest.Domain.DTO.RegistrationDTO;
import com.nuospin.javaTraining.sachinTest.Exception.RequiredFieldException;

public interface UserService {

	default public RegistrationDTO register(RegistrationDTO registrationDto){
		if(registrationDto.Validate(registrationDto.getUsername(), registrationDto.getPassword(), registrationDto.getEmailId()))
		{
			//perform database operation
			return registrationDto;
		}
		
		
		throw new RequiredFieldException("please enter the required field");
	}
	
	
	
}
