package com.nuospin.javaTraining.generic;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class GenericDemo<T> {
	private T obj1;
	private T obj2;
	

	public GenericDemo() {
		super();
		// TODO Auto-generated constructor stub
	}


	public GenericDemo(T obj1, T obj2) {
		super();
		this.obj1 = obj1;
		this.obj2 = obj2;
	}


	public T getObj1() {
		return obj1;
	}


	public void setObj1(T obj1) {
		this.obj1 = obj1;
	}


	public T getObj2() {
		return obj2;
	}


	public void setObj2(T obj2) {
		this.obj2 = obj2;
	}


	public static void main(String[] args) {
		GenericDemo<String> genericDemo = new GenericDemo<String>();
		genericDemo.setObj1("genericClassDemo");
		System.out.println(genericDemo.getObj1());
		
/*		ArrayList<String> list = new ArrayList<String>();
		list.add("abc");
		String str = list.get(0);
		System.out.println(str);*/

	}

}
