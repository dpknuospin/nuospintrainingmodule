package com.nuospin.javaTraining.generic;

public class GenericArrayDemo {
	private static <E> void printArrayElements(E[] elements){
		for (E e : elements) {
			System.out.println(e);
			
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String [] stringArray = {"pooja","sukhpal","satvinder"};
		printArrayElements(stringArray);
		
		Integer [] integerArray = {1,2,3};
		printArrayElements(integerArray);

	}

}
