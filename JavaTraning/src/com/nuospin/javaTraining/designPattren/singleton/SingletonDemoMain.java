package com.nuospin.javaTraining.designPattren.singleton;

public class SingletonDemoMain {

	public static void main(String[] args) {
		SingletonDemo  singletonDemo = SingletonDemo.getInstance();
		SingletonDemo  singletonDemo1 = SingletonDemo.getInstance();
		System.out.println(singletonDemo.hashCode());
		System.out.println(singletonDemo1.hashCode());
		System.out.println(singletonDemo.equals(singletonDemo1));

	}

}
