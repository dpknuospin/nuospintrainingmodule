package com.nuospin.javaTraining.designPattren.singleton;

public class SingletonDemo {
	private static SingletonDemo INSTANCE = null;
	// private static final SingletonDemo INSTANCE = new SingletonDemo();

	public static SingletonDemo getInstance() {
		// if you take INSTANCE as final just remove if block

		if (INSTANCE == null) {
			INSTANCE = new SingletonDemo();
		}
		return INSTANCE;

	}

	private SingletonDemo() {
		super();
	}

}
