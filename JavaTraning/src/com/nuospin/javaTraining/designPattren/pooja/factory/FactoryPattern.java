package com.nuospin.javaTraining.designPattren.pooja.factory;

public class FactoryPattern {
	public static void main(String[] args) {

		Shape rectangle = ShapeFactory.getShape("Rectangle");
		rectangle.draw();
		Shape circle = ShapeFactory.getShape("Circle");
		circle.draw();
		Shape square = ShapeFactory.getShape("Square");
		square.draw();
	}
}