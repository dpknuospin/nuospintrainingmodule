package com.nuospin.javaTraining.designPattren.factory;

public interface LoginService {
	 public void login(String userId, String password);

}
