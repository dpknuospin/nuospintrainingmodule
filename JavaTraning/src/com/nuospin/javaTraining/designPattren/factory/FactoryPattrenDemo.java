package com.nuospin.javaTraining.designPattren.factory;

public class FactoryPattrenDemo {
	public static void main (String args[]) {
		LoginService facebookLoginService = LoginFactoryService.getInstence("facebook");
		facebookLoginService.login("admin", "admin");
		LoginService googleLoginService = LoginFactoryService.getInstence("google");
		googleLoginService.login("google", "noPassword");
		
	}

}
