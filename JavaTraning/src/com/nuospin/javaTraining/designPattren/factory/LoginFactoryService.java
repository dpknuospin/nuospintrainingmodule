package com.nuospin.javaTraining.designPattren.factory;

public class LoginFactoryService {
	
	public static LoginService getInstence(String type){
		if(type == null){
			return null;
		}
		if(type.equalsIgnoreCase("facebook")){
			return new FacebookLoginService();
		}
		else if (type.equalsIgnoreCase("google")) {
			return new GoogleLoginService();
			
		}
		return null;
		
	}

}
