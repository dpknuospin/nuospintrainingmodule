package com.nuospin.javaTraining.designPattren.DTO;

public class RegistrationDto {

	private String name;
	private String password;
	private String email;

	public RegistrationDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public RegistrationDto(String name, String password, String email) {
		super();
		this.name = name;
		this.password = password;
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "RegistrationDto [name=" + name + ", password=" + password + ", email=" + email + "]";
	}

	public Boolean isValidate(RegistrationDto registrationDto) {
		if (nameValidation(registrationDto.getName()) && passwordValidation(registrationDto.getPassword())
				&& emailValidation(registrationDto.getEmail())) {
			return true;
		}
		return false;

	}

	private boolean emailValidation(String email) {
		return isStringNullOrEmpty(email);
	}

	private boolean passwordValidation(String password) {
		return isStringNullOrEmpty(password);
	}

	private Boolean nameValidation(String name) {
		return isStringNullOrEmpty(name);

	}

	private Boolean isStringNullOrEmpty(String string) {
		if (string != null && string.length() > 0) {
			return true;
		}
		return false;
	}

}
