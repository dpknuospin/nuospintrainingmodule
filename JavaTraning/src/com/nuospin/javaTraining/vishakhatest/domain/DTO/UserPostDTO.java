package com.nuospin.javaTraining.vishakhatest.domain.DTO;

public class UserPostDTO
{
		private Long pid;
		private Long uid;
		private String description;

		public UserPostDTO() {
			super();
			// TODO Auto-generated constructor stub
		}
		public UserPostDTO(Long pid, Long uid, String description)
		{
			super();
			this.pid = pid;
			this.uid = uid;
			this.description = description;
		}
		public Long getPid() {
			return pid;
		}
		public void setPid(Long pid) {
			this.pid = pid;
		}
		public Long getUid() {
			return uid;
		}
		public void setUid(Long uid) {
			this.uid = uid;
		}
		public String getDescription() {
			return description;
		}
		public void setDescription(String description) {
			this.description = description;
		}

		public Boolean isPostValidate(Long pid, Long uid, String description) {
			if (PidValidation(getPid()) && DescriptionValidation(getDescription())
					&& UidValidation(getUid())) {
				return true;
			}
			return false;

		}

		private Boolean PidValidation(Long pid) {
			if (pid != null && pid > 0) {
				return true;
			}
			return false;
		}
		private Boolean UidValidation(Long uid)
		{
			if (uid != null && uid > 0) {
				return true;
			}
			return false;
		}

		private Boolean DescriptionValidation(String description)
		{
			if (description != null && description.length() > 0) {
				return true;
			}
			return false;
		}
}
	

