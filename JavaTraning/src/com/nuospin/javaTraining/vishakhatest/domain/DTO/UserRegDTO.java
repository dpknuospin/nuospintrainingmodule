package com.nuospin.javaTraining.vishakhatest.domain.DTO;

public class UserRegDTO {

	private String Name;
	private String Email_Id;
	private String Password;

	public UserRegDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UserRegDTO(String Name, String Email_Id, String Password) {
		super();
		this.Name = Name;
		this.Email_Id = Email_Id;
		this.Password = Password;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		this.Name = name;
	}

	public String getPassword() {
		return Password;
	}

	public void setPassword(String password) {
		this.Password = password;
	}

	public String getEmail() {
		return Email_Id;
	}

	public void setEmail(String email) {
		this.Email_Id = email;
	}

	@Override
	public String toString() {
		return "RegDTO [name=" + Name + ", email=" + Email_Id + " , password=" + Password + "]";
	}

	public Boolean isValidate(UserRegDTO reg) {
		if (nameValidation(reg.getName()) && passwordValidation(reg.getPassword())
				&& emailValidation(reg.getEmail())) {
			return true;
		}
		return false;

	}

	private boolean emailValidation(String email) {
		return isStringNullOrEmpty(email);
	}

	private boolean passwordValidation(String password) {
		return isStringNullOrEmpty(password);
	}

	private Boolean nameValidation(String name) {
		return isStringNullOrEmpty(name);

	}

	private Boolean isStringNullOrEmpty(String string) {
		if (string != null && string.length() > 0) {
			return true;
		}
		return false;
	}

}

