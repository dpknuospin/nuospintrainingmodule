package com.nuospin.javaTraining.vishakhatest.domain;

import java.util.Date;

public class UserPost 
{
	private String uid;
	private String pid;
	private String caption;
	private String urlLink;
	private String description;
	private Date createdDate;
	private Date modifiedDate;
	
	public UserPost() {
		super();
		// TODO Auto-generated constructor stub
	}
	public UserPost(String pid, String uid, String caption, String urlLink, String description, Date createdDate, Date modifiedDate)
	{
		super();
		this.pid = pid;
		this.uid = uid;
		this.caption = caption;
		this.urlLink = urlLink;
		this.description = description;
		this.createdDate = createdDate;
		this.modifiedDate = modifiedDate;
	}
	public String getPid() {
		return pid;
	}
	public void setPid(String pid) {
		this.pid = pid;
	}
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public String getCaption() {
		return caption;
	}
	public void setCaption(String caption) {
		this.caption = caption;
	}
	public String getUrlLink() {
		return urlLink;
	}
	public void setUrlLink(String urlLink) {
		this.urlLink = urlLink;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getCreated() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	
}
