package com.nuospin.javaTraining.vishakhatest.domain;


public class UserReg {

	private String Name;
	private String Profile_Pic;
	private String Address;
	private Gen gender;
	private Integer Phone_No;
	private Integer Age;
	private String Email_Id;
	private String Password;
	
	public UserReg()
	{
		super();
	}
	public UserReg(String Name, String Profile_Pic, String Address, Gen gender, Integer Phone_No, Integer Age, String Email_Id, String Password )
	{
		super();
		this.Name = Name;
		this.Profile_Pic = Profile_Pic;
		this.Address = Address;
		this.gender = gender;
		this.Phone_No = Phone_No;
		this.Age = Age;
		this.Email_Id = Email_Id;
		this.Password = Password;
	}

	public String getName() {
		return Name;
	}
	public void setName(String Name) {
		this.Name = Name;
	}
	public String getProfile_Pic() {
		return Profile_Pic;
	}
	public void setProfile_Pic(String Profile_Pic) {
		this.Profile_Pic = Profile_Pic;
	}
	
	public String getAddress() {
		return Address;
	}
	public void setAddress(String address) {
		this.Address = address;
	}
	public Integer getPhone_No() {
		return Phone_No;
	}
	public void setPhone_No(Integer Phone_No) {
		this.Phone_No = Phone_No;
	}
	
	public Gen getGender() {
		return gender;
	}
	public void setGender(Gen gender) {
		this.gender= gender;
	}
	
	public Integer getAge() {
		return Age;
	}
	public void setAge(Integer age) {
		this.Age = age;
	}
	public String getEmail() {
		return Email_Id;
	}
	public void setEmail(String email) {
		this.Email_Id = email;
	}
	public String getPassword() {
		return Password;
	}
	public void setPassword(String password) {
		this.Password = password;
	}

}


