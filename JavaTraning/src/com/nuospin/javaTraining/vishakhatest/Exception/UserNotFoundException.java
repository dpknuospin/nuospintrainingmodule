package com.nuospin.javaTraining.vishakhatest.Exception;

public class UserNotFoundException extends Exception

{

		public UserNotFoundException()
		{
			super();
		}

		public UserNotFoundException(String message)
		{
			super(message);
			System.out.println(message);
		}

}

