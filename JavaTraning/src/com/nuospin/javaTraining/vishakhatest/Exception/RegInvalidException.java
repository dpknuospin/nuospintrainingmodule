package com.nuospin.javaTraining.vishakhatest.Exception;


public class RegInvalidException extends Exception
{
	public RegInvalidException() 
	{
		super();
		System.out.println("custom exception is caught");
	}

	public RegInvalidException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
		System.out.println(message);
	}
}
