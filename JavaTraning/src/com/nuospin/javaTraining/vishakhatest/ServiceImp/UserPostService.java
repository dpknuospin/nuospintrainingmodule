package com.nuospin.javaTraining.vishakhatest.ServiceImp;

import com.nuospin.javaTraining.vishakhatest.Exception.RegInvalidException;
import com.nuospin.javaTraining.vishakhatest.domain.DTO.UserPostDTO;

public class UserPostService
{
	
		
		public UserPostDTO register(Long pid, Long uid, String description) throws RegInvalidException
		{
			UserPostDTO p_ob = new UserPostDTO();
			if(p_ob.isPostValidate(pid, uid, description))
			{
				return  saveToDb(p_ob);
			}
			else
			{
				// Invalid RegistrationDto exception can be thrown
				throw new RegInvalidException("Invalid Registration");
			}
			
			
		}

		private UserPostDTO saveToDb(UserPostDTO p_ob) {
			// saves user registration details in database
			return p_ob;
		}

	

}
