package com.nuospin.javaTraining.vishakhatest.ServiceImp;

import com.nuospin.javaTraining.vishakhatest.Exception.UserNotFoundException;

public interface LoginService 
{
	public void LoginEmail(String userId, String password) throws UserNotFoundException;
	public void LoginGoogle(String userId, String password) throws UserNotFoundException;
	public void LoginFacebook(String userId, String password) throws UserNotFoundException;
	public void LoginTwitter(String userId, String password) throws UserNotFoundException;
}
		