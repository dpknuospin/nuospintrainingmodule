package com.nuospin.javaTraining.vishakhatest.ServiceImp;

import com.nuospin.javaTraining.vishakhatest.Exception.RegInvalidException;
import com.nuospin.javaTraining.vishakhatest.domain.DTO.UserRegDTO;


public class UserRegService  {
	
	public UserRegDTO register(UserRegDTO reg) throws RegInvalidException
	{
		UserRegDTO reg_ob = new UserRegDTO();
		if(reg_ob.isValidate(reg)){
			return  saveToDb(reg);
		}
		else{
			// Invalid RegistrationDto exception can be thrown
			throw new RegInvalidException("Invalid Registration");
		}
		
		
	}

	private UserRegDTO saveToDb(UserRegDTO reg) {
		// saves user registration details in database
		return reg;
	}

}