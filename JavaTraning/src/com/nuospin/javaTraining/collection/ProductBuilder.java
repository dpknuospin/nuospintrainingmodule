package com.nuospin.javaTraining.collection;
import java.util.ArrayList;
import java.util.Collections;

public class ProductBuilder {
	
	public static void main(String args[]){
		Product product1 = new Product("mobile", "nokia", Integer.valueOf(4000), Integer.valueOf(4),"skuNoika");
		Product product2 = new Product("mobile", "nokia", Integer.valueOf(2000), Integer.valueOf(3),"skuNoika");
		Product product3 = new Product("mobile", "nokia", Integer.valueOf(1000), Integer.valueOf(5),"skuNoika");
		Product product4 = new Product("mobile", "nokia", Integer.valueOf(3000), Integer.valueOf(5),"skuNoika");
		ArrayList<Product> productSet = new ArrayList<Product>();
		productSet.add(product1);
		productSet.add(product2);
		productSet.add(product3);
		productSet.add(product4);
		Collections.sort(productSet);
		System.out.println(productSet);
	}

}
