package com.nuospin.javaTraining.collection;

public class Employee {
//Employee class 
	private String EmpName;
	private String EmpAddress;
	private Integer EmpId;
	private Integer EmpSalary;

	public String getEmpName() {
		return EmpName;
	}

	public void setEmpName(String empName) {
		EmpName = empName;
	}

	public String getEmpAddress() {
		return EmpAddress;
	}

	public void setEmpAddress(String empAddress) {
		EmpAddress = empAddress;
	}

	public Integer getEmpId() {
		return EmpId;
	}

	public void setEmpId(Integer empId) {
		EmpId = empId;
	}

	public Integer getEmpSalary() {
		return EmpSalary;
	}

	public void setEmpSalary(Integer empSalary) {
		EmpSalary = empSalary;
	}

}
