package com.nuospin.javaTraining.collection;

public class Relation {
//main class added
	public static void main(String[] args) {
		Employee emp1 = new Employee();
		emp1.setEmpName("sachin");
		emp1.setEmpAddress("Greater Noida");
		emp1.setEmpId(123);
		emp1.setEmpSalary(1000);

		Employee emp2 = new Employee();
		emp2.setEmpName("shivam");
		emp2.setEmpAddress("Delhi");
		emp2.setEmpId(032);
		emp2.setEmpSalary(2132);

		Employee emp3 = new Employee();
		emp3.setEmpName("zyz");
		emp3.setEmpAddress("Gurugram");
		emp3.setEmpId(007);
		emp3.setEmpSalary(52232);
		
		Employee emp4 = new Employee();
		emp4.setEmpName("Prashant");
		emp4.setEmpAddress("Mohan Nagar, Ghaziabad");
		emp4.setEmpId(062);
		emp4.setEmpSalary(41121);
		
		Company cc = new Company();
		cc.setCompanyName("NuoSpin");
		cc.setAddress("Sushant Lok, Gurugram");
		cc.setComRegNumber(2016);

		cc.getEmployee().add(emp1);
		cc.getEmployee().add(emp2);
		cc.getEmployee().add(emp3);
		cc.getEmployee().add(emp4);
		
		System.out.println(cc.getEmployee().isEmpty());
		System.out.println(cc.getEmployee().size());
		cc.getEmployee().remove(2);
		System.out.println(cc.getEmployee().get(1).getEmpId());
		System.out.println(cc.getEmployee().get(1).getEmpName());
		System.out.println(cc.getEmployee().get(1).getEmpAddress());
		System.out.println(cc.getEmployee());

	}
}
