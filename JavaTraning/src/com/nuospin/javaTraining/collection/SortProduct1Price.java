package com.nuospin.javaTraining.collection;

import java.util.Comparator;

public class SortProduct1Price implements Comparator<Product1> {

	@Override
	public int compare(Product1 o1, Product1 o2) {
		return o1.getPrice() -o2.getPrice();
	}

}
