package com.nuospin.javaTraining.collection;

public class ShashankRelation {
	public static void main(String[] args) {
		ShashankEmployee emp1 = new ShashankEmployee();
		emp1.setEmpName("sachin");
		emp1.setCompany("Greater Noida");
		emp1.setPosition("Developer");
		emp1.setAddress("Delhi");

		ShashankEmployee emp2 = new ShashankEmployee();
		emp2.setEmpName("shivam");
		
		emp2.setCompany("Delhi");
		emp2.setPosition("Tester");
		emp2.setAddress("Noida");

		ShashankEmployee emp3 = new ShashankEmployee();
		emp3.setEmpName("Rahul");
		emp3.setCompany("Gurgaon");
		emp3.setPosition("Developer");
		emp3.setAddress("Aligarh");

		ShashankCompany cc = new ShashankCompany();
		cc.setCompanyName("NuoSpin");
		cc.setLocation("Sushant Lok");
		cc.setRegNumber("AB123");

		cc.getEmployee().add(emp1);
		cc.getEmployee().add(emp2);
		cc.getEmployee().add(emp3);
		
		System.out.println(cc.getEmployee().isEmpty());
		System.out.println(cc.getEmployee().size());
		System.out.println(cc.getEmployee().get(0).getEmpName());
		System.out.println(cc.getEmployee().get(0).getCompany());
		System.out.println(cc.getEmployee().get(0).getAddress());
		System.out.println(cc.getEmployee().get(0).getPosition());
		System.out.println(cc.getEmployee().get(1).getEmpName());
		System.out.println(cc.getEmployee().get(1).getCompany());
		System.out.println(cc.getEmployee().get(1).getAddress());
		System.out.println(cc.getEmployee().get(1).getPosition());
		System.out.println(cc.getEmployee().get(2).getEmpName());
		System.out.println(cc.getEmployee().get(2).getCompany());
		System.out.println(cc.getEmployee().get(2).getAddress());
		System.out.println(cc.getEmployee().get(2).getPosition());

	}
}


