package com.nuospin.javaTraining.collection;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

public class SetDemo {
	
	
	
	public static void main(String args[]){
		Set hasSetDemo = new HashSet();
		hasSetDemo.add(1);
		hasSetDemo.add(2);
		hasSetDemo.remove(1);
		hasSetDemo.add(4);
		hasSetDemo.add("sachin");
		System.out.println(hasSetDemo);
		System.out.println("isEmpty" +hasSetDemo.isEmpty());
		System.out.println("contains" +hasSetDemo.contains(2));
		Integer array [] ={1,1,3,3,4};
		Set<Integer> uniqueArray = new HashSet<Integer>();
		
		for (Integer integer : array) {
			uniqueArray.add(integer);
		}
		
		System.out.println(uniqueArray);
	
		Iterator iterator = hasSetDemo.iterator();
		
		while(iterator.hasNext()){
			System.out.println("elements"+iterator.next());
		}
		
		String str1 = new String("sukhpal");
		String str2 = new String("sukhpal");
		
		Set<String> set = new HashSet<String>();
		set.add(str1);
		set.add(str2);
		System.out.println("set"+set);
		
		Set<Integer> treeSet = new TreeSet<Integer>();
		treeSet.add(6);
		treeSet.add(2);
		System.out.println("treeSet"+ treeSet);

		
		
		
		
	}

}
