package com.nuospin.javaTraining.collection;

import java.util.ArrayList;
import java.util.List;

public class ListDemo {
	
	public static void main(String args[]) {
		
		List<String> list = new ArrayList<String>();
		list.add("sachin");
		list.add("rahul");
		list.add("sachin");
		list.remove(2);
		list.add(1, "sachin");
		System.out.println(list.size());
		System.out.println(list);
		
		
	}

}
