package com.nuospin.javaTraining.collection;

public class Post {
	
	



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((imageUrl == null) ? 0 : imageUrl.hashCode());
		result = prime * result + ((metaData == null) ? 0 : metaData.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Post other = (Post) obj;
		if (imageUrl == null) {
			if (other.imageUrl != null)
				return false;
		} else if (!imageUrl.equals(other.imageUrl))
			return false;
		
		return true;
	}

	@Override
	public String toString() {
		return "Post [name=" + name + ", imageUrl=" + imageUrl + ", metaData=" + metaData + "]";
	}

	private String name;
	
	private String imageUrl;
	
	private Object metaData;
	

	public Post() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Post(String name, String imageUrl, Object metaData) {
		super();
		this.name = name;
		this.imageUrl = imageUrl;
		this.metaData = metaData;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public Object getMetaData() {
		return metaData;
	}

	public void setMetaData(Object metaData) {
		this.metaData = metaData;
	}
	
	
	
	
	
	

}
