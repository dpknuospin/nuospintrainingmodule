package com.nuospin.javaTraining.collection.Rahul;
import java.util.ArrayList;
import java.util.Collections;
public class Factory {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Item item1 = new Item("mobile", "nokia", Integer.valueOf(4000), Integer.valueOf(4),"jan","Dec");
		Item item2 = new Item("laptop", "dell", Integer.valueOf(2000), Integer.valueOf(3),"dec","Jan");
		Item item3 = new Item("charger", "samsung", Integer.valueOf(1000), Integer.valueOf(5),"june","May");
		Item item4 = new Item("calci", "casio", Integer.valueOf(3000), Integer.valueOf(5),"feb","Mar");
		ArrayList<Item> productSet = new ArrayList<Item>();
		productSet.add(item1);
		productSet.add(item2);
		productSet.add(item3);
		productSet.add(item4);
		Collections.sort(productSet);
		System.out.println(productSet);
	}

	}

