package com.nuospin.javaTraining.collection.Rahul;



public class Item implements Comparable<Item>{
	private String name;
	
	private String brand;
	
	private Integer price;
	
	private Integer skuCode;
	
	private String mfgdate;
	
	private String expdate;
	
	


	public Item(String name, String brand, Integer price, Integer skuCode,
			String mfgdate, String expdate) {
		super();
		this.name = name;
		this.brand = brand;
		this.price = price;
		this.skuCode = skuCode;
		this.mfgdate = mfgdate;
		this.expdate = expdate;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getBrand() {
		return brand;
	}


	public void setBrand(String brand) {
		this.brand = brand;
	}


	public Integer getPrice() {
		return price;
	}


	public void setPrice(Integer price) {
		this.price = price;
	}


	public Integer getSkuCode() {
		return skuCode;
	}


	public void setSkuCode(Integer skuCode) {
		this.skuCode = skuCode;
	}


	public String getMfgdate() {
		return mfgdate;
	}


	public void setMfgdate(String mfgdate) {
		this.mfgdate = mfgdate;
	}


	public String getExpdate() {
		return expdate;
	}


	public void setExpdate(String expdate) {
		this.expdate = expdate;
	}


	@Override
	public int compareTo(Item o) {
		// TODO Auto-generated method stub
		return this.price-o.price;
	}


	@Override
	public String toString() {
		return "Item [name=" + name + ", brand=" + brand + ", price=" + price
				+ ", skuCode=" + skuCode + ", Mfgdate=" + mfgdate
				+ ", Expdate=" + expdate + "]";
	}
	
}
