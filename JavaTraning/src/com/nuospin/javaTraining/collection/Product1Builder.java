package com.nuospin.javaTraining.collection;

import java.util.ArrayList;
import java.util.Collections;

public class Product1Builder {

	public static void main(String[] args) {
		Product1 product1 = new Product1("mobile", "nokia", Integer.valueOf(4000), Integer.valueOf(4),"skuNoika");
		Product1 product2 = new Product1("mobile", "nokia", Integer.valueOf(2000), Integer.valueOf(3),"skuNoika");
		Product1 product3 = new Product1("mobile", "nokia", Integer.valueOf(1000), Integer.valueOf(5),"skuNoika");
		Product1 product4 = new Product1("mobile", "nokia", Integer.valueOf(3000), Integer.valueOf(5),"skuNoika");
		ArrayList<Product1> productSet = new ArrayList<Product1>();
		productSet.add(product1);
		productSet.add(product2);
		productSet.add(product3);
		productSet.add(product4);
		Collections.sort(productSet, new SortProduct1Price());
		System.out.println(productSet);

	}

}
