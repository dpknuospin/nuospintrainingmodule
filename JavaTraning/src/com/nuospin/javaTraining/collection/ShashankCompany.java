package com.nuospin.javaTraining.collection;
import java.util.*;
// company class
public class ShashankCompany {
	private String CompanyName;
	private String Location;
	private String RegNumber;
	
	private ArrayList<ShashankEmployee> employee= new ArrayList<ShashankEmployee>();

	public String getCompanyName() {
		return CompanyName;
	}

	public void setCompanyName(String companyName) {
		CompanyName = companyName;
	}

	public String getLocation() {
		return Location;
	}

	public void setLocation(String location) {
		Location = location;
	}

	public String getRegNumber() {
		return RegNumber;
	}

	public void setRegNumber(String regNumber) {
		RegNumber = regNumber;
	}

	public ArrayList<ShashankEmployee> getEmployee() {
		return employee;
	}

	public void setEmployee(ArrayList<ShashankEmployee> employee) {
		this.employee = employee;
	}

	
	}
	

	