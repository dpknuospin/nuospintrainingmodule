package com.nuospin.javaTraining.collection;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class HashCodeEquals {

	public static void main(String args[]) {
		Post post1 = new Post("post2", "post76.jpg", "metadata");
		Post post2 = new Post("post2", "post3.jpg", "metadata");
		Post post3 = new Post("post3", "post3.jpg", "metadata");
		Post post4 = new Post("post4", "post3.jpg", "metadata");
		System.out.println(post1.hashCode());
		System.out.println(post2.hashCode());
		
		Set<Post> set = new HashSet<Post>();
	/*	Map<Post, Object> map = new HashMap<Post,Object>();
		map.put(post1, "firstPost");
		map.put(post2, "firstPost");
		map.put(post3, "firstPost");
		map.put(post3, "firstPost");
		System.out.println(map);
		*/
		
		set.add(post1);
		set.add(post2);
		set.add(post3);
		set.add(post4);
		for (Post post : set) {
			System.out.println(post);
			
		}
	}

}
