package com.nuospin.javaTraining.collection;

import java.util.*;
// Company base class
public class Company {

	private String CompanyName;	
	private String Address;
	private Integer ComRegNumber;

	private ArrayList<Employee> employee = new ArrayList<Employee>();

	public ArrayList<Employee> getEmployee() {
		return employee;
	}

	public void setEmployee(ArrayList<Employee> employee) {
		this.employee = employee;
	}

	public String getCompanyName() {
		return CompanyName;
	}

	public void setCompanyName(String companyName) {
		CompanyName = companyName;
	}

	public String getAddress() {
		return Address;
	}

	public void setAddress(String address) {
		Address = address;
	}

	public Integer getComRegNumber() {
		return ComRegNumber;
	}

	public void setComRegNumber(Integer comRegNumber) {
		ComRegNumber = comRegNumber;
	}

}
