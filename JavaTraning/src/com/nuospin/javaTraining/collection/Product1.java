package com.nuospin.javaTraining.collection;

public class Product1 {
	private String name;
	
	private String brand;
	
	private Integer price;
	
	private Integer size;
	
	private String skuCode;

	public Product1(String name, String brand, Integer price, Integer size, String skuCode) {
		super();
		this.name = name;
		this.brand = brand;
		this.price = price;
		this.size = size;
		this.skuCode = skuCode;
	}

	public Product1() {
		super();
		// TODO Auto-generated constructor stub
	}
	

	@Override
	public String toString() {
		return "Product [name=" + name + ", brand=" + brand + ", price=" + price + ", size=" + size + ", skuCode="
				+ skuCode + "]";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	public Integer getSize() {
		return size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}

	public String getSkuCode() {
		return skuCode;
	}

	public void setSkuCode(String skuCode) {
		this.skuCode = skuCode;
	}
	

}
