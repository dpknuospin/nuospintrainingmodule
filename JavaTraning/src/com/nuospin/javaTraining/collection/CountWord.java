package com.nuospin.javaTraining.collection;

import java.util.HashMap;
import java.util.Map;

public class CountWord {
	
	Map<String, Integer> wordCount(String sentence){
	Map<String,Integer> map = new HashMap<String,Integer>();
	String [] wordArray = sentence.split(" ");
	for (String word : wordArray) {
		if(map.containsKey(word)){
			Integer count = map.get(word);
			count++;
			map.put(word,count);
		}
		else{
			map.put(word,1);
		}
		
	}
		return map;
		
	}
	public static void  main(String args[]) {
		CountWord countWord = new CountWord();
		System.out.println(countWord.wordCount("this is a this demo a demo a demo this is"));
		
	}

}
