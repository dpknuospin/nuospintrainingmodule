package com.nuospin.javaTraining.collection;

import java.util.HashMap;
import java.util.Map;

public class MapDemo {
	
	public static void main(String args[]){
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("first", "str");
		map.put("second", 2);
		map.put("first",1);
		String str = "hello this is hello";
		String [] array = str.split(" ");
		for (String string : array) {
			System.out.println(string);
		}
		System.out.println(map.get("first"));
		System.out.println(map.keySet());
		System.out.println(map.entrySet());
		
	}
	

}
