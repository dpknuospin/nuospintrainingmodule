package com.nuospin.javaTraining.collection;

import java.util.ArrayList;

public class Company_Rahul {

	private String comName;
	private String location;
	private long regNumber;
	
	ArrayList<Employee_Rahul> al= new ArrayList();
	public ArrayList<Employee_Rahul> getAl() {
		return al;
	}
	public void setAl(ArrayList<Employee_Rahul> al) {
		this.al = al;
	}
	
	public String getComName(){
		return comName;
	}
	public void SetComName(String comName){
		this.comName= comName;
	}
	
	public String getLocation(){
		return location;
	}
	public void SetLocation(String location){
		this.location= location;
	}
	public long getRegNumber(){
		return regNumber;
	}
	public void SetRegNumber(Long regNumber){
		this.regNumber= regNumber;
	}
}
