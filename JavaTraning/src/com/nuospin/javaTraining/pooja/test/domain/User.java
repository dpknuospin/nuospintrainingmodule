package com.nuospin.javaTraining.pooja.test.domain;

import java.util.HashSet;
import java.util.Set;

import com.nuospin.javaTraining.pooja.test.domain.enm.Gender;

public class User {
	private Long id;
	private String firstName;
	private String lastName;
	private Integer mobileNumber;
	private String email;
	private String password;
	private String profilePicUrl;
	private Gender gender;
	private Set<Long> outgoingFriendRequestIds = new HashSet<Long>();
	private Set<Long> incomingfriendRequestIds = new HashSet<Long>();
	private Set<Long> friendIds = new HashSet<Long>();
	public User(Long id, String firstName, String lastName, Integer mobileNumber, String email, String password,
			String profilePicUrl, Gender gender) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.mobileNumber = mobileNumber;
		this.email = email;
		this.password = password;
		this.profilePicUrl = profilePicUrl;
		this.gender = gender;
	}
	public User() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Set<Long> getIncomingfriendRequestIds() {
		return incomingfriendRequestIds;
	}
	public void setIncomingfriendRequestIds(Set<Long> incomingfriendRequestIds) {
		this.incomingfriendRequestIds = incomingfriendRequestIds;
	}
	
	public Set<Long> getOutgoingFriendRequestIds() {
		return outgoingFriendRequestIds;
	}
	public void setOutgoingFriendRequestIds(Set<Long> outgoingFriendRequestIds) {
		this.outgoingFriendRequestIds = outgoingFriendRequestIds;
	}
	public Set<Long> getFriendIds() {
		return friendIds;
	}
	public void setFriendIds(Set<Long> friendIds) {
		this.friendIds = friendIds;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public Integer getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(Integer mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getProfilePicUrl() {
		return profilePicUrl;
	}
	public void setProfilePicUrl(String profilePicUrl) {
		this.profilePicUrl = profilePicUrl;
	}
	public Gender getGender() {
		return gender;
	}
	public void setGender(Gender gender) {
		this.gender = gender;
	}
	

}
