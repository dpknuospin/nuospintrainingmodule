package com.nuospin.javaTraining.pooja.test.domain;

import java.util.Date;

public class Post {
	private Long id;
	private String title;
	private String imageUrl;
	private Long ownerId;
	private String description;
	private Date dateCreated;
	private Date dateModified;
	
	
	public Post(Long id, String title, String imageUrl, Long ownerId, String description, Date dateCreated,
			Date dateModified) {
		super();
		this.id = id;
		this.title = title;
		this.imageUrl = imageUrl;
		this.ownerId = ownerId;
		this.description = description;
		this.dateCreated = dateCreated;
		this.dateModified = dateModified;
	}
	public Post() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public Long getOwnerId() {
		return ownerId;
	}
	public void setOwnerId(Long ownerId) {
		this.ownerId = ownerId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}
	public Date getDateModified() {
		return dateModified;
	}
	public void setDateModified(Date dateModified) {
		this.dateModified = dateModified;
	}
	

}
