package com.nuospin.javaTraining.pooja.test.domain.dto;

import java.util.Date;

public class PostDto {
	private Long id;
	private String title;
	private String imageUrl;
	private Long ownerId;
	private String description;
	private Date dateCreated;
	private Date dateModified;
	public PostDto() {
		super();
		// TODO Auto-generated constructor stub
	}
	public PostDto(Long id, String title, String imageUrl, Long ownerId, String description, Date dateCreated,
			Date dateModified) {
		super();
		this.id = id;
		this.title = title;
		this.imageUrl = imageUrl;
		this.ownerId = ownerId;
		this.description = description;
		this.dateCreated = dateCreated;
		this.dateModified = dateModified;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public Long getOwnerId() {
		return ownerId;
	}
	public void setOwnerId(Long ownerId) {
		this.ownerId = ownerId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}
	public Date getDateModified() {
		return dateModified;
	}
	public void setDateModified(Date dateModified) {
		this.dateModified = dateModified;
	}
	
	public Boolean isValidPostDto(String title, Long ownerId, String description){
		if(isTitleValid (title) && isOwnerIdValid(ownerId) && isDescriptionValid(description)){
			return true;
		}
		return false;
	}
	
	private boolean isTitleValid(String title) {
		if (title != null && title.length() > 0) {
			return true;
		}
		return false;
	}
	private boolean isOwnerIdValid(Long ownerId) {
		if (ownerId != null && ownerId > 0) {
			return true;
		}	return false;
	}
	private boolean isDescriptionValid(String description) {
		if (description != null && description.length() > 0) {
			return true;
		}
		return false;
	}
}
