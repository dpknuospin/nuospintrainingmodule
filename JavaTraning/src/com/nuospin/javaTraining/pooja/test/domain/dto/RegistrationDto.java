package com.nuospin.javaTraining.pooja.test.domain.dto;

import com.nuospin.javaTraining.pooja.test.domain.enm.Gender;

public class RegistrationDto {
	private String firstName;
	private String lastName;
	private Integer mobileNumber;
	private String email;
	private String password;
	private String profilePicUrl;
	private Gender gender;

	public RegistrationDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public RegistrationDto(String firstName, String lastName, Integer mobileNumber, String email, String password,
			String profilePicUrl, Gender gender) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.mobileNumber = mobileNumber;
		this.email = email;
		this.password = password;
		this.profilePicUrl = profilePicUrl;
		this.gender = gender;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Integer getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(Integer mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getProfilePicUrl() {
		return profilePicUrl;
	}

	public void setProfilePicUrl(String profilePicUrl) {
		this.profilePicUrl = profilePicUrl;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public Boolean isValidRegistrationDto(String firstName, String email, String password) {

		if (isFirstNameValid(firstName) && isEmailValid(email) && isPasswordValid(password)) {
			return true;
		}

		return false;

	}

	private Boolean isFirstNameValid(String firstName) {
		if (firstName != null && firstName.length() > 0) {
			return true;
		}
		return false;
	}

	private Boolean isEmailValid(String email) {
		if (email != null && email.length() > 0) {
			return true;
		}
		return false;
	}

	private Boolean isPasswordValid(String password) {
		if (password != null && password.length() > 0 && password.length() < 8) {
			return true;
		}
		return false;
	}
}
