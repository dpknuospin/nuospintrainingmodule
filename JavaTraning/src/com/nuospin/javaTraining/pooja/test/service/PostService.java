package com.nuospin.javaTraining.pooja.test.service;


import com.nuospin.javaTraining.pooja.test.domain.dto.PostDto;
import com.nuospin.javaTraining.pooja.test.exception.InvalidFieldsException;

public interface PostService {
	public PostDto createPost(PostDto postDto) throws InvalidFieldsException;

}
