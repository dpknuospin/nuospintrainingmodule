package com.nuospin.javaTraining.pooja.test.service;

import com.nuospin.javaTraining.pooja.test.exception.UserNotFoundException;

public interface FriendService {
	public Boolean sendRequest(Long userId, Long friendId) throws UserNotFoundException;

}
