package com.nuospin.javaTraining.pooja.test.service;

import com.nuospin.javaTraining.pooja.test.domain.User;
import com.nuospin.javaTraining.pooja.test.domain.dto.LoginDto;
import com.nuospin.javaTraining.pooja.test.exception.UserNotFoundException;

public class LoginService implements UserService{

	@Override
	public User login(LoginDto loginDto) throws UserNotFoundException {
		User user = findbyEmailAndPassword(loginDto.getEmail(),loginDto.getPassword());
		if (user != null){
			return user;
		}
		throw new UserNotFoundException("Invalid email or password");
	}

	private User findbyEmailAndPassword(String email, String password) {
		// return user if user found in db by given email and passowrd
		return null;
	}
	

}
