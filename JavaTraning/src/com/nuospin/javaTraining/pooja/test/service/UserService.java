package com.nuospin.javaTraining.pooja.test.service;


import com.nuospin.javaTraining.pooja.test.domain.User;
import com.nuospin.javaTraining.pooja.test.domain.dto.LoginDto;
import com.nuospin.javaTraining.pooja.test.domain.dto.RegistrationDto;
import com.nuospin.javaTraining.pooja.test.exception.InvalidFieldsException;
import com.nuospin.javaTraining.pooja.test.exception.UserNotFoundException;
import com.nuospin.javaTraining.shashankTest.Exception.RegFailException;

public interface UserService {
 default public RegistrationDto register(RegistrationDto registrationDto) throws InvalidFieldsException{
	 if(registrationDto.isValidRegistrationDto(registrationDto.getFirstName(),registrationDto.getEmail(), registrationDto.getPassword())){
		 //save registration details  into database and return saved details.
		 return registrationDto;
	 }
	throw new InvalidFieldsException("Missing required fields");
 }
 
 public User login(LoginDto loginDto) throws UserNotFoundException;
 
}
