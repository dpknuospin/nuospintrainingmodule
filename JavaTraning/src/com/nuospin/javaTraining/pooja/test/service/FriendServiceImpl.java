package com.nuospin.javaTraining.pooja.test.service;

import com.nuospin.javaTraining.pooja.test.domain.User;
import com.nuospin.javaTraining.pooja.test.exception.UserNotFoundException;

public class FriendServiceImpl implements FriendService {

	@Override
	public Boolean sendRequest(Long userId, Long friendId) throws UserNotFoundException {
		// TODO Auto-generated method stub
		User user = findUserById(userId);
		User friend = findUserById(friendId);
		if (user != null && friend != null) {
			friend.getIncomingfriendRequestIds().add(userId);
			user.getOutgoingFriendRequestIds().add(friendId);
			return true;
		}
		throw new UserNotFoundException("User not found");
	}

	public User findUserById(Long id) {
		// find user by id from db and return.
		return null;

	}

}
