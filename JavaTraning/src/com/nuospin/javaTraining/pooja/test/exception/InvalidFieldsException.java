package com.nuospin.javaTraining.pooja.test.exception;

public class InvalidFieldsException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4730256751847643440L;

	public InvalidFieldsException() {
		super();
	}

	public InvalidFieldsException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

}
