package com.nuospin.javaTraining.pooja.java8;

import java.util.Arrays;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.stream.Collectors;

public class StatisticsDemo {

	public static void main(String[] args) {
		List<Integer> numbers = Arrays.asList(3, 2, 2, 3, 7, 3, 5);
		// get list of unique squares
		List<Integer> squaresList = numbers.stream().map(i -> i * i).distinct().collect(Collectors.toList());
		List<Integer> oddNumbers = numbers.stream().filter(i -> i % 2 != 0).distinct().collect(Collectors.toList());
		List<Integer> first5elements = Arrays.asList(7, 55, 4, 8, 34, 0, 17, 16, 06, 11, 58, 43, 30, 6, 9).stream().limit(4).collect(Collectors.toList());
		List<Integer> sortedElements = Arrays.asList(7, 55, 4, 8, 34, 0, 16, 06, 11, 58, 43).stream().sorted().collect(Collectors.toList());
		List<String> forEachElements = Arrays.asList("ab", "bc", "rt");
		forEachElements.forEach(System.out::println);
		long parallelProcessing = Arrays.asList("ayz", "", "pm", "a", "", "dp", "xyz", "tl").parallelStream().filter(string -> string.isEmpty()).count();
		IntSummaryStatistics Summary = Arrays.asList(7, 55, 4, 8, 34, 0, 17, 16, 06, 11, 58, 43, 30, 6, 9).stream().mapToInt((x) -> x).summaryStatistics();
		System.out.println("List of squares of numbers: " + squaresList);
		System.out.println("List of odd numbers: " + oddNumbers);
		System.out.println("list of first 5 elements: " + first5elements);
		System.out.println("List of sorted elements: " + sortedElements);
		System.out.println("Count of empty string: " + parallelProcessing);
		System.out.println("Maximum value in list: " + Summary.getMax());
		System.out.println("Minimum value in list: " + Summary.getMin());
		System.out.println("Total numbers in list: " + Summary.getCount());
		System.out.println("Sum of numbers are: " + Summary.getSum());
		System.out.println("Average of numbers are:	" + Summary.getAverage());

	}
}
