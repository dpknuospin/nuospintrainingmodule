package com.nuospin.javaTraining.designPattern.Pooja.DTO;

public class Product {
	private String title;
	private String brand;
	private Integer sku;
	private String description;
	private Integer price;
	private String colour;
	private Size size;
	private String materail;
	public Product() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Product(String title, String brand, Integer sku, String description, Integer price, String colour, Size size,
			String materail) {
		super();
		this.title = title;
		this.brand = brand;
		this.sku = sku;
		this.description = description;
		this.price = price;
		this.colour = colour;
		this.size = size;
		this.materail = materail;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public Integer getSku() {
		return sku;
	}
	public void setSku(Integer sku) {
		this.sku = sku;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getPrice() {
		return price;
	}
	public void setPrice(Integer price) {
		this.price = price;
	}
	public String getColour() {
		return colour;
	}
	public void setColour(String colour) {
		this.colour = colour;
	}
	public Size getSize() {
		return size;
	}
	public void setSize(Size size) {
		this.size = size;
	}
	public String getMaterail() {
		return materail;
	}
	public void setMaterail(String materail) {
		this.materail = materail;
	}
	
	

}
