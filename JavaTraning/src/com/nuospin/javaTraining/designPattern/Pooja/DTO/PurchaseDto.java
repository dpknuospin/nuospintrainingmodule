package com.nuospin.javaTraining.designPattern.Pooja.DTO;

public class PurchaseDto {
	private String title;
	private String brand;
	private Integer sku;
	private Integer price;

	public PurchaseDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PurchaseDto(String title, String brand, Integer sku, Integer price) {
		super();
		this.title = title;
		this.brand = brand;
		this.sku = sku;
		this.price = price;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public Integer getSku() {
		return sku;
	}

	public void setSku(Integer sku) {
		this.sku = sku;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "PurchaseDto [title=" + title + ", brand=" + brand + ", sku=" + sku + ", price=" + price + "]";
	}

	public Boolean isValidate(PurchaseDto purchaseDto) {
		if (titleValidation(purchaseDto.getTitle()) && brandValidation(purchaseDto.getBrand())
				&& skuValidation(purchaseDto.getSku()) && priceValidation(purchaseDto.getPrice())) {
			return true;
		}
		return false;

	}

	private boolean titleValidation(String title) {
		return isStringNullOrEmpty(title);
	}

	private boolean brandValidation(String brand) {
		return isStringNullOrEmpty(brand);

	}

	private boolean isStringNullOrEmpty(String string) {
		if (string != null && string.length() > 0) {
			return true;
		}
		return false;
	}

	private boolean skuValidation(Integer sku) {
		return isIntegerNull(sku);
	}

	private boolean priceValidation(Integer price) {
		return isIntegerNull(price);
	}

	private boolean isIntegerNull(Integer integer) {
		if (integer != null) {
			return true;
		}
		return false;
	}

}
