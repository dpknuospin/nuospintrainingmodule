package com.nuospin.javaTraining.designPattern.Pooja.DTO;

public class PurchaseService {

	public PurchaseDto purchase(PurchaseDto purchaseDto) {
		PurchaseDto purchaseDtoObj = new PurchaseDto();
		if (purchaseDtoObj.isValidate(purchaseDto)) {
			return saveToDb(purchaseDto);
		} else {
			// Invalid RegistrationDto exception can be thrown
		}
		return null;

	}

	private PurchaseDto saveToDb(PurchaseDto purchaseDto) {
		// this method saves user registration details in db
		return purchaseDto;
	}

}
