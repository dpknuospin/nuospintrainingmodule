package com.nuospin.javaTraining.domain;

public class PoojaService {

	public static void main(String args[]) {
		Pooja top = new Pooja();
		top.setBrand("Mango");
		top.setSize("Small");
		top.setColour("Black");
		top.setMaterial("Cotton");
		top.setPrice(2000);
		System.out.println(top.getBrand());
		System.out.println(top.getSize());
		System.out.println(top.getColour());
		System.out.println(top.getMaterial());
		System.out.println(top.getPrice());

		Pooja bag = new Pooja("Baggit", "Medium", "Pink", "Leather", 4999);
		System.out.println("\n\nBrand= " + bag.getBrand() + "\nSize= " + bag.getSize() + "\nColour= "
				+ bag.getColour() + "\nPrice= " + bag.getPrice());
		
		Pooja shoe= new Pooja("Nike", "US6", 3789);
		System.out.println("\n\nBrand= " +shoe.getBrand() +"\nSize= "+shoe.getSize() +"\nPrice= "+shoe.getPrice());
		

	}

}
