package com.nuospin.javaTraining.domain;

public class UserService {
	
	public static void main(String args[]){
		User user = new User();
		user.setName("deepak");
		user.setEmail("deepak@nuospin.com");
		user.setContactNumber((long) 1234567890);
		user.setAddress("c block");
		
		System.out.println(user.getName());
		System.out.println(user.hashCode());
		User dummyUser = new User("pooja", "pooja@nuospin.com", "c block", (long) 12345678);
		System.out.println("name=" +dummyUser.getName() +" email=" +dummyUser.getEmail());
	}

}
