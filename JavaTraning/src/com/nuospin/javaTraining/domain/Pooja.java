package com.nuospin.javaTraining.domain;

public class Pooja {
	private String Brand;
	private String Size;
	private String Colour;
	private String Material;
	private Integer Price;

	public Pooja() {
		super();
	}

	public Pooja(String Brand, String Size, String Colour, String Material, Integer Price) {
		super();
		this.Brand = Brand;
		this.Size = Size;
		this.Colour = Colour;
		this.Material = Material;
		this.Price = Price;
	}
	

	public Pooja(String brand, String size, Integer price) {
		super();
		Brand = brand;
		Size = size;
		Price = price;
	}

	public String getBrand() {
		return Brand;
	}

	public void setBrand(String brand) {
		Brand = brand;
	}

	public String getSize() {
		return Size;
	}

	public void setSize(String size) {
		Size = size;
	}

	public String getColour() {
		return Colour;
	}

	public void setColour(String colour) {
		Colour = colour;
	}

	public String getMaterial() {
		return Material;
	}

	public void setMaterial(String material) {
		Material = material;
	}

	public Integer getPrice() {
		return Price;
	}

	public void setPrice(Integer price) {
		Price = price;
	}

}
