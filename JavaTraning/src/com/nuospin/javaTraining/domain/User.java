package com.nuospin.javaTraining.domain;

public class User {
	private String name;
	private String email;
	private String address;
	private Long contactNumber;

	public User() {
		super();
		// TODO Auto-generated constructor stub
	}

	public User(String name, String email, String address, Long contactNumber) {
		super();
		this.name = name;
		this.email = email;
		this.address = address;
		this.contactNumber = contactNumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Long getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(Long contactNumber) {
		this.contactNumber = contactNumber;
	}

}
