package com.nuospin.javaTraining.divyatest.DTO;

public class PostDTO {
	public PostDTO() {
		// TODO Auto-generated constructor stub
	}
	private Long id;
	private String title;
	private Long ownerId;
	private String description;
	public PostDTO(Long id, String title, Long ownerId, String description) {
		super();
		this.id = id;
		this.title = title;
		this.ownerId = ownerId;
		this.description = description;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Long getOwnerId() {
		return ownerId;
	}
	public void setOwnerId(Long ownerId) {
		this.ownerId = ownerId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Boolean isValidPostDto(String title, Long ownerId, String description){
		if(isTitleValid (title) && isOwnerIdValid(ownerId) && isDescriptionValid(description)){
			return true;
		}
		return false;
	}
	
	private boolean isTitleValid(String title) {
		if (title != null && title.length() > 0) {
			return true;
		}
		return false;
	}
	private boolean isOwnerIdValid(Long ownerId) {
		if (ownerId != null && ownerId > 0) {
			return true;
		}	return false;
	}
	private boolean isDescriptionValid(String description) {
		if (description != null && description.length() > 0) {
			return true;
		}
		return false;
	}

}
