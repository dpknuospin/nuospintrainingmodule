package com.nuospin.javaTraining.divyatest.DTO;

public class LoginDTO {

	private String emailId;
	private String password;
	public LoginDTO() {
	}
	public LoginDTO(String emailId, String password) {
		super();
		this.emailId = emailId;
		this.password = password;
	}
	public String getEmail() {
		return emailId;
	}
	public void setEmail(String emailId) {
		this.emailId = emailId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	

}
