package com.nuospin.javaTraining.divyatest.Service;
import com.nuospin.javaTraining.divyatest.Exceptions.UserNotFoundException;

public interface LoginService {
	public void loginEmail(String emailId, String password) throws UserNotFoundException;
	public void loginFacebook(String userId, String password) throws UserNotFoundException;
	public void loginGoogle(String userId, String password) throws UserNotFoundException;
	public void loginTwitter(String userId, String password) throws UserNotFoundException;
}
