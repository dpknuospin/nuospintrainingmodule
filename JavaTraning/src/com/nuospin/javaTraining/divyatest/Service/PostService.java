package com.nuospin.javaTraining.divyatest.Service;

import com.nuospin.javaTraining.divyatest.Domain.Comment;
import com.nuospin.javaTraining.divyatest.Domain.Post;

public class PostService {
	
	public Integer getNumComments(Post p){
		return p.getComments().size();
	}
	public void showComment(Post p){
		for(Comment c: p.getComments()){
			System.out.println(c.getComment());
		}
	}
}
