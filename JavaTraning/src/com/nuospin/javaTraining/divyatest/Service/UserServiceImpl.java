package com.nuospin.javaTraining.divyatest.Service;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import com.nuospin.javaTraining.divyatest.Domain.Comment;
import com.nuospin.javaTraining.divyatest.Domain.Group;
import com.nuospin.javaTraining.divyatest.Domain.Post;
import com.nuospin.javaTraining.divyatest.Domain.User;

public class UserServiceImpl implements UserService{
	public User findUserById(Long id) {
		// find user by id from db and return.
		return null;

	}
	public void showPost(User u){
	    	while(u.getPosts().iterator().hasNext()){
	    		System.out.println(u.getPosts().iterator().next());
	    	}
	    }
    public void sendFriendRequest(User u,User friend){
	    	acceptFriendRequest(u,friend);
	}
	public void acceptFriendRequest(User u,User friend){
	        System.out.println("Do you want to accept "+u.getName()+" as your friend? (Yes/No)");
	        //if yes
	    	    u.setFriends(friend);;            	
	 }
	 public void commentPost(Post p){
	    	Comment c= new Comment();
	    	c.setComment("Comment");
	    	p.getComments().add(c);
	  }
     public void likePost(Post p) {
    	int like=p.getNumLikes().intValue()+1;
 		p.setNumLikes(like);
     }
	 public void sharePost(Post p, User u) {
			u.getMyPosts().push(p);
	 }
	 public void filterFriend() {
	 }
	 public void getMostPopularFriend(User u) {
		  	Comparator<User> byNumOfFriends = (u1, u2) -> Integer.compare(u1.getFriends().size(), u2.getFriends().size());
		  	HashMap<User,Integer> popularFriends = new HashMap<User,Integer>();
		  	List<User> popFriend=u.getFriends().stream().sorted(byNumOfFriends).collect(Collectors.toList());
		  	for (User key: popFriend)
		  	{
		  		int rate=0;
		  		popularFriends.put(key,++rate);
			 }	
			 System.out.println(popularFriends);    
	 }
	 public void createGroup(User u) {
		 	u.getMyGroups().add(new Group());
	 }
	 public Post createPost(User u){
		 Post p=new Post();
		 u.getMyPosts().push(p);
		 return p;
	 }
		
}


