package com.nuospin.javaTraining.divyatest.Service;
import com.nuospin.javaTraining.divyatest.Exceptions.UserNotFoundException;
import com.nuospin.javaTraining.divyatest.Service.LoginService;

public class LoginServiceImpl implements LoginService {

		public void loginEmail(String emailId, String password) throws UserNotFoundException {
			if (emailId.equals("admin") && password.equals("123456p")) {
				System.out.println("Successfully login");
			} else {
				throw new UserNotFoundException("Please enter correct emailId or password");
			}
	    }
		public void loginFacebook(String userId, String password) throws UserNotFoundException {
			if (userId.equals("admin") && password.equals("123456p")) {
				System.out.println("Successfully login");
			} else {
				throw new UserNotFoundException("Please enter correct userId and password");
			}
	    }
		public void loginGoogle(String userId, String password) throws UserNotFoundException {
			if (userId.equals("admin") && password.equals("123456p")) {
				System.out.println("Successfully login");
			} else {
				throw new UserNotFoundException("Please enter correct userId and password");
			}
	    }
		public void loginTwitter(String userId, String password) throws UserNotFoundException {
			if (userId.equals("admin") && password.equals("123456p")) {
				System.out.println("Successfully login");
			} else {
				throw new UserNotFoundException("Please enter correct userId and password");
			}
	    }
}
