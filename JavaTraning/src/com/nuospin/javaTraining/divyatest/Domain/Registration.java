package com.nuospin.javaTraining.divyatest.Domain;

import com.nuospin.javaTraining.divyatest.Domain.Gender;

public class Registration {
	public Registration() {

	}
	private String name;
	private Integer mobileNumber;
	private String email;
	private String password;
	private String profilePicUrl;
	private Gender gender;
	public Registration(String name, Integer mobileNumber, String email, String password,
			String profilePicUrl, Gender gender) {
		super();
		this.name = name;
		this.mobileNumber = mobileNumber;
		this.email = email;
		this.password = password;
		this.profilePicUrl = profilePicUrl;
		this.gender = gender;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	
	public Integer getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(Integer mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getProfilePicUrl() {
		return profilePicUrl;
	}

	public void setProfilePicUrl(String profilePicUrl) {
		this.profilePicUrl = profilePicUrl;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public Boolean isValidRegistrationDto(String name, String email, String password) {

		if (isFirstNameValid(name) && isEmailValid(email) && isPasswordValid(password)) {
			return true;
		}

		return false;

	}

	private Boolean isFirstNameValid(String name) {
		if (name != null && name.length() > 0) {
			return true;
		}
		return false;
	}

	private Boolean isEmailValid(String email) {
		if (email != null && email.length() > 0) {
			return true;
		}
		return false;
	}

	private Boolean isPasswordValid(String password) {
		if (password != null && password.length() > 0 && password.length() < 8) {
			return true;
		}
		return false;
	}
}
