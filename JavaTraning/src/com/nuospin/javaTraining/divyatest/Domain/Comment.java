package com.nuospin.javaTraining.divyatest.Domain;

public class Comment {
	private String comment;
	private Long uid;
	private Long pid;
	private Long id;
	
	public Comment() {
		// TODO Auto-generated constructor stub
	}
	public Comment(Long uid,Long pid,Long id,String comment){
		this.uid=uid;
		this.pid=pid;
		this.id=id;
		this.comment=comment;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
}
