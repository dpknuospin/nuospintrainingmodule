package com.nuospin.javaTraining.Exception;

public class LoginDemo {
	private static void login(String userId, String password) throws UserNotFoundException {
		if (userId.equals("pooja") && password.equals("123456p")) {
			System.out.println("Successfully login");
		} else {
			throw new UserNotFoundException("Please enter correct userId and password");
		}
	}

	public static void main(String[] args) throws UserNotFoundException {
		login("poo", "123456p");
	}

}

class UserNotFoundException extends Exception {

	public UserNotFoundException() {
		super();
	}

	public UserNotFoundException(String message) {
		super(message);
		System.out.println(message);
	}

}
