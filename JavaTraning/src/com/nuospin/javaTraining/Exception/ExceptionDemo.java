package com.nuospin.javaTraining.Exception;


public class ExceptionDemo{
	private static void underSixteenTeamSelection(int age) throws AgeNotValidException{
		if(age>16){
			throw new AgeNotValidException("age not valid, its greater than 16 so");
		}
		else{
			System.out.println("selected");
		}
		
	}
	public static void main(String args[]) throws AgeNotValidException{
		underSixteenTeamSelection(17);
		
	}
	
}

 class AgeNotValidException extends Exception {

	public AgeNotValidException() {
		super();
		System.out.println("custom exception is caught");
	}

	public AgeNotValidException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
		System.out.println(message);
	}

}
