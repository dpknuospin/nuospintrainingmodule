package com.nuospin.javaTraining.rahulTest.Service;
import com.nuospin.javaTraining.rahulTest.Service.Impl.FacebookLogin;
import com.nuospin.javaTraining.rahulTest.Service.Impl.GoogleLogin;
import com.nuospin.javaTraining.rahulTest.Service.Impl.TwitterLogin;
public class LoginFactoryService {
	public static LoginService getInstence(String type){
		if(type == null){
			return null;
		}
		if(type.equalsIgnoreCase("facebook")){
			return new FacebookLogin();
		}
		else if (type.equalsIgnoreCase("google")) {
			return new GoogleLogin();
			
		}
		else if (type.equalsIgnoreCase("twiier")) {
			return new TwitterLogin();
			
		}
		return null;
		
	}

}
