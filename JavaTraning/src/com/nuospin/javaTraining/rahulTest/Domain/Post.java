package com.nuospin.javaTraining.rahulTest.Domain;

public class Post {
private String pid;
private String uid;
private String content;
public String getPid() {
	return pid;
}
public void setPid(String pid) {
	this.pid = pid;
}
public String getUid() {
	return uid;
}
public void setUid(String uid) {
	this.uid = uid;
}
public String getContent() {
	return content;
}
public void setContent(String content) {
	this.content = content;
}

}
