package com.nuospin.javaTraining.rahulTest.Domain;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.nuospin.javaTraining.designPattren.DTO.Address;
import com.nuospin.javaTraining.designPattren.DTO.Gender;
import com.nuospin.javaTraining.rahulTest.Application.MyGroup;

public class User {
	private String name;
	private Integer phoneNumber;
	private String email;
	private String password;
	private Address address;
	private Gender gender;
	private String profilePicUrl; 
	private Integer age;
	public List<Friends> friends= new ArrayList<Friends>();
	public Set<MyGroup> myGroup = new HashSet<MyGroup>();
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Set<MyGroup> getMyGroup() {
		return myGroup;
	}
	public void setMyGroup(Set<MyGroup> myGroup) {
		this.myGroup = myGroup;
	}
	public Integer getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(Integer phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getEmail() {
		return email;
	}
	public List<Friends> getFriends() {
		return friends;
	}
	public void setFriends(List<Friends> friends) {
		this.friends = friends;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public Gender getGender() {
		return gender;
	}
	public void setGender(Gender gender) {
		this.gender = gender;
	}
	public String getProfilePicUrl() {
		return profilePicUrl;
	}
	public void setProfilePicUrl(String profilePicUrl) {
		this.profilePicUrl = profilePicUrl;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	
	
}
