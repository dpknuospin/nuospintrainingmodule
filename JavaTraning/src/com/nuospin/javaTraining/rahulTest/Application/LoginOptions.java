package com.nuospin.javaTraining.rahulTest.Application;

import com.nuospin.javaTraining.rahulTest.Exception.InvalidCredentialsException;
import com.nuospin.javaTraining.rahulTest.Service.LoginFactoryService;
import com.nuospin.javaTraining.rahulTest.Service.LoginService;

public class LoginOptions {
public void loginMenu(String username,String pass) throws InvalidCredentialsException{
	InvalidCredentialsException ice = new InvalidCredentialsException();
	if ((ice.isValidateCredentials(username,pass))== true)
		{
	LoginService facebookLogin = LoginFactoryService.getInstence("facebook");
	facebookLogin.login(username, pass);
	LoginService googleLogin = LoginFactoryService.getInstence("google");
	googleLogin.login(username, pass);
	LoginService twitterLogin = LoginFactoryService.getInstence("twitter");
	twitterLogin.login(username, pass);
	}
	
	else throw new InvalidCredentialsException("Invalid Credentials");
}
}
