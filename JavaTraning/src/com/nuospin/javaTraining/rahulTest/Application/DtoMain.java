package com.nuospin.javaTraining.rahulTest.Application;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import com.nuospin.javaTraining.rahulTest.Domain.User;
import com.nuospin.javaTraining.rahulTest.Dto.RegDto;
import com.nuospin.javaTraining.rahulTest.Exception.InvalidCredentialsException;
import com.nuospin.javaTraining.rahulTest.Service.RegService;

public class DtoMain {
	public static void main(String[] args) throws InvalidCredentialsException, IOException {
		RegDto regDto = new RegDto("9876", "rahulrajput18@gmail.com");
		RegService regService = new RegService();
		System.out.println(regService.register(regDto));
		
		System.out.println("You can login either via facebook, google or twitter");
		BufferedReader ob=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter username and password: ");
		String username= ob.readLine();
		String pass= ob.readLine();
		LoginOptions loginOptions= new LoginOptions();
		loginOptions.loginMenu(username,pass);
		
		User user = new User();
		AddDetails addDetails = new AddDetails();
		addDetails.addFriends(user);
		
		
		
}
}

