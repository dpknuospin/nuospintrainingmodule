package com.nuospin.javaTraining.rahulTest.Dto;

public class PostDto {
	private String pid;
	private String uid;
	public PostDto(String pid, String uid) {
		super();
		this.pid = pid;
		this.uid = uid;
	}
	public PostDto(){
		super();
	}
	public String getPid() {
		return pid;
	}
	public void setPid(String pid) {
		this.pid = pid;
	}
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}	
public boolean isValidate(PostDto postDto){
	if (passwordValidation(postDto.getPid())
			&& emailValidation(postDto.getUid())) {
		return true;
	}
	return false;
	
}
private boolean emailValidation(String pid) {
	return isStringNullOrEmpty(pid);
}

private boolean passwordValidation(String uid) {
	return isStringNullOrEmpty(uid);
}
private Boolean isStringNullOrEmpty(String string) {
	if (string != null && string.length() > 0) {
		return true;
	}
	return false;
}
}
