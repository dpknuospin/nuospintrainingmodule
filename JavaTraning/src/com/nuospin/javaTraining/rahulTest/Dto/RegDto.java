package com.nuospin.javaTraining.rahulTest.Dto;
public class RegDto {
	private String password;
	private String email;

	public RegDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public RegDto(String password, String email) {
		super();
		this.password = password;
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "RegistrationDto [password=" + password + ", email=" + email + "]";
	}

	public Boolean isValidate(RegDto regDto) {
		if (passwordValidation(regDto.getPassword())
				&& emailValidation(regDto.getEmail())) {
			return true;
		}
		return false;

	}

	private boolean emailValidation(String email) {
		return isStringNullOrEmpty(email);
	}

	private boolean passwordValidation(String password) {
		return isStringNullOrEmpty(password);
	}

	private Boolean isStringNullOrEmpty(String string) {
		if (string != null && string.length() > 0) {
			return true;
		}
		return false;
	}

}
