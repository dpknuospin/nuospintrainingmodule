package sachin_pack;

public class Sachin
{
	private String name;
	private Integer rollNo;
	private Integer age;
	private String fatherName;
	
	public Sachin()
	{
	}
	
	public Sachin(String name,Integer rollNo, Integer age, String fatherName)
	{
		this.name=name;
		this.rollNo=rollNo;
		this.age=age;
		this.fatherName=fatherName;
	}
	
	public String getName()
	{	
		return name;
	}

	public void setName(String name)
	{
		this.name=name;
	}
	
	public Integer getAge()
	{
		return age;
	}
	
	public void setAge(Integer age)
	{
		this.age=age;
	}
	
	public Integer getrollNo()
	{
		return rollNo;
	}
	public void setrollNo(Integer rollNo)
	{
		this.rollNo=rollNo;
	}
	public String getfatherName()
	{
		return fatherName;
	}
	
	public void setFather_name(String FatherName)
	{
		this.fatherName=FatherName;
	}
	
}


