package sachin_pack;

public class SachinComparable implements Comparable
{

	Integer stuid;
	
	SachinComparable(int stuid)
	{
		this.stuid=stuid;
	}
	
	
	public String toString(){
		
		return "Student "+ stuid;
	}
	
	
	@Override
	public int compareTo(Object obj) {
		
		int stuid1=this.stuid;
		
		SachinComparable s2 = (SachinComparable)obj;
		
		int stuid2= s2.stuid;
		 if(stuid1  > stuid2)
			 return -1;
		 
		 else if(stuid1 < stuid2 )
			 return 1;
		 else 
			 return 0;
	}
	
			 
		 
		
	}



