package sachin_pack;

import java.util.TreeSet;
public class SachinMain {

	public static void main(String[] args) {
		
		SachinComparable SC1= new SachinComparable(50);
		SachinComparable SC2= new SachinComparable(100);
		SachinComparable SC3= new SachinComparable(150);
		SachinComparable SC4= new SachinComparable(200);
	
		TreeSet ts1 = new TreeSet();
		
		ts1.add(SC1);
		ts1.add(SC2);
		ts1.add(SC3);
		ts1.add(SC4);
		
		System.out.println(ts1);
		
		TreeSet ts2 = new TreeSet(new SachinComparator());
		ts2.add(SC1);
		ts2.add(SC2);
		ts2.add(SC3);
		ts2.add(SC4);			
		
		System.out.println(ts2);
		
	}
	

}
