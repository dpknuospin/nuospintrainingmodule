package sachin_pack;

import java.util.Comparator;

public class SachinComparator implements Comparator {

	@Override
	public int compare(Object obj1, Object obj2) {
	
		
		
	SachinComparable SC1 =(SachinComparable)obj1;
	SachinComparable SC2 =(SachinComparable)obj2;

		return SC2.compareTo(SC1);
	
}

}
